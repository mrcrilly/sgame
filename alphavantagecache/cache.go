package alphavantagecache

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

//type AlphaVantageCache map[string]alphavantage.StockResponse

func Get(symbol, function string) ([]byte, bool) {
	filename := "./.alphavantage.cache/" + symbol + "." + function
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return nil, false
	}

	fd, err := os.Open(filename)
	if err != nil {
		return nil, false
	}

	defer fd.Close()
	everything, err := ioutil.ReadAll(fd)
	if err != nil {
		panic(err)
	}
	return everything, true
}

func Add(symbol, function string, response interface{}) error {
	filename := "./.alphavantage.cache/" + symbol + "." + function
	fd, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, 0660)
	if err != nil {
		panic(err)
	}

	defer fd.Close()
	encoder := json.NewEncoder(fd)
	return encoder.Encode(response)
}
