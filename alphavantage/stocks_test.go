package alphavantage

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestTimeSeriesIntraday(t *testing.T) {
	Convey("I can get a symbol's intraday quotes", t, func() {
		r := TimeSeriesIntraday(AlphaVantageAPIKey, "JIN.AX", Interval15, OutputCompact, DataTypeJSON, true)
		So(r.TimeSeries15, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeries15 {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesIntradayWithoutCache(t *testing.T) {
	Convey("I can get a symbol's intraday quotes without using the cache", t, func() {
		r := TimeSeriesIntraday(AlphaVantageAPIKey, "JIN.AX", Interval15, OutputCompact, DataTypeJSON, false)
		So(r.TimeSeries15, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeries15 {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesDaily(t *testing.T) {
	Convey("I can get a symbol's daily quotes", t, func() {
		r := TimeSeriesDaily(AlphaVantageAPIKey, "JIN.AX", Interval15, OutputCompact, DataTypeJSON, true)
		So(r.TimeSeriesDaily, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeriesDaily {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesDailyAdjusted(t *testing.T) {
	Convey("I can get a symbol's daily adjusted quotes", t, func() {
		r := TimeSeriesDailyAdjusted(AlphaVantageAPIKey, "JIN.AX", Interval15, OutputCompact, DataTypeJSON, true)
		So(r.TimeSeriesDaily, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeriesDaily {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesWeekly(t *testing.T) {
	Convey("I can get a symbol's weekly quotes", t, func() {
		r := TimeSeriesWeekly(AlphaVantageAPIKey, "JIN.AX", Interval15, OutputCompact, DataTypeJSON, true)
		So(r.TimeSeriesWeekly, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeriesWeekly {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesWeeklyAdjusted(t *testing.T) {
	Convey("I can get a symbol's weekly adjusted quotes", t, func() {
		r := TimeSeriesWeeklyAdjusted(AlphaVantageAPIKey, "JIN.AX", Interval15, OutputCompact, DataTypeJSON, true)
		So(r.TimeSeriesWeeklyAdjusted, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeriesWeeklyAdjusted {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesMonthly(t *testing.T) {
	Convey("I can get a symbol's monthly quotes", t, func() {
		r := TimeSeriesMonthly(AlphaVantageAPIKey, "JIN.AX", DataTypeJSON, true)
		So(r.TimeSeriesMonthly, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeriesMonthly {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})
}

func TestTimeSeriesMonthlyAdjusted(t *testing.T) {
	Convey("I can get a symbol's monthly adjusted quotes", t, func() {
		r := TimeSeriesMonthlyAdjusted(AlphaVantageAPIKey, "JIN.AX", DataTypeJSON, true)
		So(r.TimeSeriesMonthlyAdjusted, ShouldNotBeNil)

		for _, datapoint := range r.TimeSeriesMonthlyAdjusted {
			So(toFloat(datapoint.Open), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.High), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Low), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toFloat(datapoint.Close), ShouldBeGreaterThanOrEqualTo, 0.0)
			So(toInt(datapoint.Volume), ShouldBeGreaterThanOrEqualTo, 0)
		}
	})

}

func TestBatchStockQuotes(t *testing.T) {
	SkipConvey("I can get a batch of quotes", t, nil)
}
