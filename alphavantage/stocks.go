package alphavantage

import (
	"encoding/json"
	"os"

	"gitlab.com/mrcrilly/sgame/alphavantagecache"
)

const (
	AlphaVantageURL = "https://www.alphavantage.co/query"

	Interval1  = "1min"
	Interval5  = "5min"
	Interval15 = "15min"
	Interval30 = "30min"
	Interval60 = "60min"

	FunctionIntraday        = "TIME_SERIES_INTRADAY"
	FunctionDaily           = "TIME_SERIES_DAILY"
	FunctionDailyAdjusted   = "TIME_SERIES_DAILY_ADJUSTED"
	FunctionWeekly          = "TIME_SERIES_WEEKLY"
	FunctionWeeklyAdjusted  = "TIME_SERIES_WEEKLY_ADJUSTED"
	FunctionMonthly         = "TIME_SERIES_MONTHLY"
	FunctionMonthlyAdjusted = "TIME_SERIES_MONTHLY_ADJUSTED"
	FunctionBatchStockQuote = "BATCH_STOCK_QUOTES"

	OutputCompact = "compact"
	OutputFull    = "full"

	DataTypeJSON = "json"
	DataTypeCSV  = "csv"
)

var (
	AlphaVantageAPIKey = func() string {
		return os.Getenv("SGAME_ALPHAVANTAGE_API_KEY")
	}()
)

type StockResponse struct {
	MetaData map[string]string `json:"Meta Data"`

	TimeSeriesDaily           map[string]StockResponseItem         `json:"Time Series (Daily)"`
	TimeSeriesWeekly          map[string]StockResponseItem         `json:"Weekly Time Series"`
	TimeSeriesWeeklyAdjusted  map[string]StockResponseAdjustedItem `json:"Weekly Adjusted Time Series"`
	TimeSeriesMonthly         map[string]StockResponseItem         `json:"Monthly Time Series"`
	TimeSeriesMonthlyAdjusted map[string]StockResponseAdjustedItem `json:"Monthly Adjusted Time Series"`
	TimeSeries1               map[string]StockResponseItem         `json:"Time Series (1min)"`
	TimeSeries5               map[string]StockResponseItem         `json:"Time Series (5min)"`
	TimeSeries15              map[string]StockResponseItem         `json:"Time Series (15min)"`
	TimeSeries30              map[string]StockResponseItem         `json:"Time Series (30min)"`
	TimeSeries60              map[string]StockResponseItem         `json:"Time Series (60min)"`
}

type StockResponseItem struct {
	Open   string `json:"1. open"`
	High   string `json:"2. high"`
	Low    string `json:"3. low"`
	Close  string `json:"4. close"`
	Volume string `json:"5. volume"`
}

type StockResponseAdjustedItem struct {
	Open             string `json:"1. open"`
	High             string `json:"2. high"`
	Low              string `json:"3. low"`
	Close            string `json:"4. close"`
	AdjustedClose    string `json:"5. adjusted close"`
	Volume           string `json:"6. volume"`
	Dividend         string `json:"7. dividend amount"`
	SplitCoefficient string `json:"8. split coefficient"`
}

type BatchStockResponse struct {
	MetaData    map[string]string
	StockQuotes []map[string]string
}

func TimeSeriesIntraday(key, symbol, interval, outputsize, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionIntraday)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionIntraday, symbol, interval, outputsize, datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionIntraday, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func TimeSeriesDaily(key, symbol, interval, outputsize, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionDaily)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionDaily, symbol, interval, outputsize, datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionDaily, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func TimeSeriesDailyAdjusted(key, symbol, interval, outputsize, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionDailyAdjusted)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionDailyAdjusted, symbol, interval, outputsize, datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionDailyAdjusted, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func TimeSeriesWeekly(key, symbol, interval, outputsize, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionWeekly)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionWeekly, symbol, interval, outputsize, datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionWeekly, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func TimeSeriesWeeklyAdjusted(key, symbol, interval, outputsize, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionWeeklyAdjusted)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionWeeklyAdjusted, symbol, interval, outputsize, datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionWeeklyAdjusted, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func TimeSeriesMonthly(key, symbol, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionMonthly)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionMonthly, symbol, "", "", datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionMonthly, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func TimeSeriesMonthlyAdjusted(key, symbol, datatype string, cache bool) StockResponse {
	var stockresponse StockResponse

	if cache {
		cachedResponse, OK := alphavantagecache.Get(symbol, FunctionMonthlyAdjusted)
		if OK {
			err := json.Unmarshal(cachedResponse, &stockresponse)
			if err != nil {
				panic(err)
			}

			return stockresponse
		}
	}

	response, err := httpRequest(key, FunctionMonthlyAdjusted, symbol, "", "", datatype)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(response, &stockresponse)
	if err != nil {
		panic(err)
	}

	if cache {
		err = alphavantagecache.Add(symbol, FunctionMonthlyAdjusted, stockresponse)
		if err != nil {
			panic(err)
		}
	}

	return stockresponse
}

func BatchStockQuotes(key, symbols, datatype string) BatchStockResponse {
	return BatchStockResponse{}
}
