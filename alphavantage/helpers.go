package alphavantage

import (
	"github.com/pkg/errors"
	"io/ioutil"
	"net/http"
	"strconv"
)

func httpRequest(key, function, symbol, interval, outputsize, datatype string) ([]byte, error) {
	request, err := http.NewRequest(http.MethodGet, AlphaVantageURL, nil)
	if err != nil {
		return nil, err
	}

	query := request.URL.Query()
	query.Add("function", function)
	query.Add("symbol", symbol)
	query.Add("datatype", datatype)
	query.Add("apikey", key)
	query.Add("interval", interval)
	query.Add("outputsize", outputsize)

	request.URL.RawQuery = query.Encode()

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusOK {
		return nil, errors.New("bad http status code returned")
	}

	bodyAll, err := ioutil.ReadAll(response.Body)
	return bodyAll, err
}

func toFloat(this string) float64 {
	f64, _ := strconv.ParseFloat(this, 64)
	return f64
}

func toInt(this string) int64 {
	i64, _ := strconv.ParseInt(this, 10, 64)
	return i64
}
