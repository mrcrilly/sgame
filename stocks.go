package main

import (
	"fmt"
	"os"
	"time"

	av "github.com/cmckee-dev/go-alpha-vantage"
)

var prices = make(map[string][]*av.TimeSeriesValue)

func queryLatestPrice(symbol string) float64 {
	results := queryTimeSeries(symbol)
	if len(results) > 0 {
		return results[len(results)-1].Close
	}

	return 0.0
}

func queryTotalDividends(symbol string, purchaseDate time.Time) float64 {
	return 0.0
}

func queryTimeSeries(symbol string) []*av.TimeSeriesValue {
	client := av.NewClient("QW1PJQDIHF42Q4DT")
	res, err := client.StockTimeSeries(av.TimeSeriesDailyAdjusted, symbol)
	if err != nil {
		panic(err)
	}

	if _, ok := prices[symbol]; !ok {
		prices[symbol] = make([]*av.TimeSeriesValue, 0)
	}

	prices[symbol] = res
	return prices[symbol]
}

func ErrorF(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, fmt.Sprintf("%s\n", format), args...)
}
