package main

import (
	"io/ioutil"
	"os"
	"time"

	yaml "gopkg.in/yaml.v2"
)

type project struct {
	Name         string        `yaml:"name"`
	Transactions []transaction `yaml:"transactions"`
}

type transaction struct {
	Symbol string
	Date   time.Time
	Cost   float64
	Volume int
}

func loadTransactions() project {
	fd, err := os.Open("project.yml")
	if err != nil {
		panic(err)
	}

	raw, err := ioutil.ReadAll(fd)
	if err != nil {
		panic(err)
	}

	var txs project
	err = yaml.Unmarshal(raw, &txs)
	if err != nil {
		panic(err)
	}

	return txs
}
