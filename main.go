package main

import "fmt"

func main() {
	txs := loadTransactions()
	for _, t := range txs.Transactions {
		sinfo := queryLatestPrice(t.Symbol)
		thenCapital := float64(t.Volume)*t.Cost + 8.0
		nowCapital := float64(t.Volume) * sinfo
		capitalGain := calculateCapitalGain(thenCapital, nowCapital)

		fmt.Printf("Symbol: %s | Then: %.2f | Today: %.2f | Gain %.2f\n", t.Symbol, thenCapital, nowCapital, capitalGain)
	}
}

func calculateCapitalGain(originalPrice, newPrice float64) float64 {
	diff := newPrice - originalPrice
	return (diff / originalPrice) * 100.0
}
